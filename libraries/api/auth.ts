import { IParamLogin } from "@interfaces/request-post/iparam-login";
import { IFetchData } from "@interfaces/ifetch-data";

const baseUrl: string = process.env.NEXT_SERVER_API || '';

class Auth {
  private url: string;

  constructor(url: string) {
    this.url = url;
  }

  public postLogin = (params: IParamLogin): IFetchData => ({
    method: 'POST',
    params,
    uri: `${this.url}/login`,
  });
}

export default new Auth(baseUrl);