import React from 'react';
import { Layout } from '@components/antd/layout';
import './content.scss';

const { Content } = Layout;

type Props = {
    children?: any;
    page?: string;
    partials?: string;
    subPartials?: string;
}

const Contents = ({ children }: Props) => {
    return (
        <Content className="site-layout" style={{ marginTop: 64 }}>
            {children}
        </Content>
    );
};

export default Contents;