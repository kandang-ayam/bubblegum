import styled from '@emotion/styled';

export const Logos = styled.div`
  width: 25%;
`;

export const LoginWrap = styled.div`
  width: 75%;
  text-align: end;
  padding-top: 15px;
  form {
    justify-content: flex-end;
    div > div > div > div > .ant-input-affix-wrapper {
      border-radius: 13px;
    }
  }
`;

export const BtnWrapper = styled.div`
  .login {
    background-color: #f64846;
    color: #ffff;
    border: none;
    margin-right: 10px;
    border-radius: 20px;
  }
  .register {
    background-color: #f6ab3b;
    color: #ffffff;
    border: none;
    border-radius: 20px;
  }
`;