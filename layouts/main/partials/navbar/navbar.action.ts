import callAPI from '@utils/fetcher';
import Auth from '@libraries/api/auth';
import { IParamLogin } from '@interfaces/request-post/iparam-login';
import { Dispatch, SetStateAction } from 'react';
import { IFetchData } from '@interfaces/ifetch-data';

export const doLogin = async (
  params: IParamLogin,
  setLoading: Dispatch<SetStateAction<boolean>>
) => {
  const payload: IFetchData = Auth.postLogin(params);
  setLoading(true);

  try {
    const response: any = await callAPI({ ...payload });
    setLoading(false);
    return response.data;
  } catch (error) {
    setLoading(false);
    console.log(error);
  }
};
