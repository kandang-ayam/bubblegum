import React from 'react';
// import { Menu } from '@components/antd/menu';
import { Layout } from '@components/antd/layout';
import './navbar.scss';
// import Link from 'next/link';
// import { useRouter } from 'next/router';
import { Logos, LoginWrap } from './navbar.styled';
import NavbarLogin from './navbar.login';

const { Header } = Layout;

const NavBar = () => {
    // const router = useRouter();

    // const MenuLink = (link: string, label: string, key: string) => (
    //     <Menu.Item key={key}>
    //       <Link href={link}>
    //         <a className={router.pathname === link ? 'active' : undefined}>{label}</a>
    //       </Link>
    //     </Menu.Item>
    // );

    return (
        <Header style={{
            position: 'fixed',
            zIndex: 2,
            width: '100%',
            display: 'flex',
            border: '1px solid #E5E5E5',
        }}>
            <Logos>
                <img src="https://svgshare.com/i/RCz.svg" height="auto" width="160" alt="cari jodoh"/>
            </Logos>
            {/* <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                {MenuLink('/', 'HOME', '1')}
            </Menu> */}
            <LoginWrap>
                <NavbarLogin />
            </LoginWrap>
        </Header>
    );
};

export default NavBar;