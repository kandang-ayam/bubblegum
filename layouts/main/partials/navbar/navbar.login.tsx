import React, { useState } from 'react';
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { BtnWrapper } from './navbar.styled';
import { doLogin } from './navbar.action';
import { IParamLogin } from '@interfaces/request-post/iparam-login';

export const NavbarLogin = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [form] = Form.useForm();

  const onFinish = async (params: IParamLogin) => {
    const response: any = await doLogin(params, setLoading);
    if (response.access_token) {
      alert('Selamat berhasil login!')
    }
  };

  return (
    <Form form={form} name="horizontal_login" layout="inline" onFinish={onFinish}>
      <Form.Item
        name="email"
        rules={[{ required: true, message: 'Please input your email!' }]}
      >
        <Input
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="Email"
          type="email"
        />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Password"
        />
      </Form.Item>
      <Form.Item shouldUpdate={true}>
        {() => (
          <BtnWrapper>
            <Button
              type="primary"
              htmlType="submit"
              className="login"
              loading={loading}
            >
              Masuk
            </Button>
            <Button
              type="primary"
              htmlType="submit"
              className="register"
            >
              Daftar
            </Button>
          </BtnWrapper>
        )}
      </Form.Item>
    </Form>
  );
};

export default NavbarLogin;