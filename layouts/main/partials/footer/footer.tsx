import React from 'react';
import { Layout } from '@components/antd/layout';

const { Footer } = Layout;

const FooterNav = () => {
    return (
        <Footer style={{
            textAlign: 'center',
            background: '#D11204',
            padding: '8px 50px',
            color: '#ffff',
            position: 'fixed',
            width: '100%',
            bottom: 0
        }}>
            <span style={{
                fontSize: '0.8em'
            }}>All trademarks referenced here in are the properties of their respective owners.</span><br/>
            <span style={{
                fontSize: '0.8em'
            }}>© 2020 Synchroza Indonesia Dating Site All Rights Reserved.</span>

        </Footer>
    );
};

export default FooterNav;