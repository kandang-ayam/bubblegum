import { Fragment } from 'react'
import Login from '@containers/login';
import Head from 'next/head';

const IndexPage = () => (
  <Fragment>
    <Head>
      <title>Syncroza | Login</title>
    </Head>
    
    <Login />
  </Fragment>
);

export default IndexPage
