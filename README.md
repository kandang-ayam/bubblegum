# TypeScript Next.js + SSR + useSWR

Server Side Rendering boilerplate using Next.js with TypeScript.

### Run the projects

Install it and run development mode:

```bash
npm install
npm build
npm run dev
# or
yarn
yarn build
yarn dev
```

Install it and run production mode:

```bash
npm install
npm build
npm run start
# or
yarn
yarn build
yarn start
```
