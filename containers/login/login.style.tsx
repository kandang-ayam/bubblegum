import styled from '@emotion/styled';

export const LoginWrapper = styled.div`
`;

export const WelcomeWrapper = styled.div`
  margin: 15px 0;
  h1 {
    font-family: PoetsenOne-Regular;
    text-align: center;
    font-size: 2em;
    span {
      color: #f64846;
    }
  }
`;

export const FlexBox = styled.div`
  display: flex;
  width: 100%;
  margin: 25px 0 7% 0;
`;

export const Box = styled.div`
  width: 33.3%;
  text-align: center;
  img {
    width: 20%;
    height: auto;
  }
`;

export const AnimatedBox = styled.div`
  height: 300px;
  width: 200px;
  position: absolute;
  top: 28%;
  right: 5%;
  border-radius: 10px;
  background: linear-gradient(to top, #CE093E, #EFA13D);
  overflow: hidden;
  transition: width 0.5s, height 1s;
  #animated-1 {
    width: 50px;
    height: 50px;
    margin-left: 100px;
    background: #E8763B;
    animation-name: one;
    animation-duration: 2s;
    transform: translateX(-50%) translateY(-50%);
    animation-iteration-count: 100000;
  }
  @keyframes one {
    from {
      width: 50px;
      height: 50px;
    }
    to {
      width: 200px;
      height: 300px;
    }
  }
  #animated-2 {
    width: 75px;
    height: 75px;
    border-radius: 50%;
    margin-left: 20px;
    background: #E3743A;
    position: absolute;
    animation-name: two;
    animation-duration: 1s;
    transform: translateX(-50%) translateY(-50%);
    animation-iteration-count: 100000;
  }
  @keyframes two {
    from {
      width: 75px;
      height: 75px;
    }
    to {
      width: 0px;
      height:   0px;
    }
  }
  #animated-3 {
    width: 75px;
    height: 75px;
    border-radius: 50%;
    left: 7em;
    background: #E3743A;
    position: absolute;
    animation-name: three;
    animation-duration: 1s;
    transform: translateX(-50%) translateY(-50%);
    animation-iteration-count: 100000;
  }
  @keyframes three {
    from {
      width: 75px;
      height: 75px;
    }
    to {
      width: 0px;
      height:   0px;
    }
  }
  #animated-4 {
    width: 100px;
    height: 100px;
    border-radius: 50%;
    left: 10em;
    background: #E3743A;
    position: absolute;
    animation-name: four;
    animation-duration: 1s;
    transform: translateX(-50%) translateY(-50%);
    animation-iteration-count: 100000;
  }
  @keyframes four {
    from {
      width: 100px;
      height: 100px;
    }
    to {
      width: 0px;
      height:   0px;
    }
  }
  p {
    position: absolute;
    top: 5%;
    text-align: center;
    color: #ffff;
    font-weight: bold;
  }
  #img-1 {
    position: absolute;
    top: 52%;
    left: 1.2em;
    border-radius: 50%;
    padding: 2px;
    background: #FFFFFF;
    width: 70px;
    height: 70px;
  }
  #img-2 {
    position: absolute;
    top: 27%;
    left: 4em;
    border-radius: 50%;
    padding: 2px;
    background: #FFFFFF;
    width: 70px;
    height: 70px;
  }
  #img-4 {
    position: absolute;
    top: 52%;
    left: 7.4em;
    border-radius: 50%;
    padding: 2px;
    background: #FFFFFF;
    width: 70px;
    height: 70px;
  }
  #sign {
    position: absolute;
    width: 100%;
    text-align: center;
    top: 80%;
    h3 {
      color: #ffff;
      font-weight: bold;
      margin: 0;
    }
    p {
      top: 96%;
      text-align: center;
      width: 100%;
    }
  }
`;