import React from 'react';
import { LoginWrapper, WelcomeWrapper, Box, FlexBox, AnimatedBox } from './login.style';

export const Login = () => {
  return (
    <LoginWrapper>
      <img
        height="auto"
        width="100%"
        src="https://dev-synchroza.s3-ap-southeast-1.amazonaws.com/asset/jessica.svg"
        alt="cari jodoh"
      />
      <AnimatedBox>
        <div id="animated-1"></div>
        <div id="animated-2"></div>
        <div id="animated-3"></div>
        <div id="animated-4"></div>
        <p>1 Juta Orang telah menunggumu</p>
        <img src="https://synchroza.com/upload/profiles/1603172694.jpeg" id="img-1" alt="cari jodoh"/>
        <img src="https://synchroza.com/upload/profiles/1603202521.jpeg" id="img-2" alt="cari jodoh"/>
        <img src="https://synchroza.com/upload/profiles/1603202784.jpeg" id="img-4" alt="cari jodoh"/>
        <div id="sign">
          <h3>Daftar Sekarang!</h3>
          <p>www.syncroza.com</p>
        </div>
      </AnimatedBox>
      <WelcomeWrapper>
        <h1>Welcome to <span>Synchroza</span></h1>
      </WelcomeWrapper>
      <FlexBox>
        <Box>
          <img src="https://dev-synchroza.s3-ap-southeast-1.amazonaws.com/asset/icon-01.svg" alt="cari jodoh"/>
          <p>Quick sign up process under 10 minutes</p>
        </Box>
        <Box>
          <img src="https://dev-synchroza.s3-ap-southeast-1.amazonaws.com/asset/icon-02.svg" alt="cari jodoh"/>
          <p>Easy to use search messaging and notifications</p>
        </Box>
        <Box>
          <img src="https://dev-synchroza.s3-ap-southeast-1.amazonaws.com/asset/icon-03.svg" alt="cari jodoh"/>
          <p>Show you exactly what makes you click</p>
        </Box>
      </FlexBox>
    </LoginWrapper>
  );
};

export default Login;